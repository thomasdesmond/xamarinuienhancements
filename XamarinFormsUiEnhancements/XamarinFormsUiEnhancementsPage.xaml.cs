﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace XamarinFormsUiEnhancements
{
    public partial class XamarinFormsUiEnhancementsPage : ContentPage
    {
        public XamarinFormsUiEnhancementsPage()
        {
            InitializeComponent();


            var toolbarItem = new ToolbarItem()
            {
                Text = "Settings",
                Command = new Command(() => Navigation.PushAsync(new SettingsPage())),
                //Order = ToolbarItemOrder.Secondary,
                                      
            };
            var searchItem = new ToolbarItem()
            {
                Icon = "search.png",
                Command = new Command(SettingsClicked),
            };

            ToolbarItems.Add(searchItem);
            ToolbarItems.Add(toolbarItem);
        }


        void SettingsClicked()
        {
            Navigation.PushAsync(new SettingsPage());
        }
    }
}

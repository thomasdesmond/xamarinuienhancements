﻿using System;
using Xamarin.Forms;

namespace XamarinFormsUiEnhancements
{
    public class EntryLengthValidator : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry entry)
        {
            entry.TextChanged += OnEntryTextChanged;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.TextChanged -= OnEntryTextChanged;
            base.OnDetachingFrom(entry);
        }

        void OnEntryTextChanged(object sender, TextChangedEventArgs args)
        {
            double result;
            bool isValid = ((Entry)sender).Text.Length > 2;
            ((Entry)sender).TextColor = isValid ? Color.Default : Color.Red;
        }
    }
}
